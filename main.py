import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:08:01.796601
#2018-02-26 15:27:01.515863
#2018-02-26 15:51:02.063798
#2018-02-26 17:16:02.181600
#2018-02-26 18:01:01.300391
#2018-02-26 18:46:01.253767
#2018-02-26 19:35:02.542059
#2018-02-26 20:46:02.199218
#2018-02-26 21:30:04.640121
#2018-02-26 22:24:01.897511
#2018-02-26 23:35:03.100873
#2018-02-27 01:21:01.542290